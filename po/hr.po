# 
# Translators:
# marcelo cripe <marcelocripe@gmail.com>, 2022
# 
msgid ""
msgstr ""
"Project-Id-Version: 0.8.1\n"
"Report-Msgid-Bugs-To: forum-antixlinux.com\n"
"POT-Creation-Date: 2022-01-26 17:42+0100\n"
"Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022\n"
"Language-Team: Croatian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hr\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Poedit 2.3\n"

msgid ""
"\n"
"Internal error %d! Unable to highlight line - offset beyond line-end.\n"
msgstr ""
"\n"
"Interna pogreška %d! Nije moguće istaknuti liniju - pomak izvan kraja linije.\n"

msgid ""
"\n"
"No content searching performed for this file.\n"
"\n"
"Please ensure containing search was enabled, and that an expression was entered prior to starting search.\n"
msgstr ""
"\n"
"Za ovu datoteku nije izvršeno pretraživanje sadržaja.\n"
"\n"
"Provjerite je li traženje koje sadrži omogućeno i je li izraz unet prije početka pretraživanja.\n"

msgid "  Debug: %d) '%s'\n"
msgstr "  Otklanjanje pogrešaka: %d) '%s'\n"

msgid "  Debug: fn='%s'\n"
msgstr "  Otklanjanje pogrešaka: fn='%s'\n"

msgid "  Debug: tc=%d + os=%d || oe=%d\n"
msgstr "  Otklanjanje pogrešaka: tc=%d + os=%d || oe=%d\n"

msgid " %s settings auto-generated - Do not edit!"
msgstr " %s postavke automatski generirane - Ne uređivati!"

msgid " [cancelled]"
msgstr " [otkazan]"

msgid " [inv]"
msgstr " [inv]"

msgid " [missing]"
msgstr " [nedostaje]"

msgid "%1.1f KB"
msgstr "%1.1f KB"

msgid "%1.1f MB"
msgstr "%1.1f MB"

msgid "%d"
msgstr "%d"

msgid "%d bytes"
msgstr "%d bajtova"

msgid "%d file found"
msgstr "%d pronađena datoteka"

msgid "%d files found"
msgstr "%d pronađene datoteke"

msgid "%f"
msgstr "%f"

msgid "%s %s\n"
msgstr "%s %s\n"

msgid "%s Error! Unable to duplicate string vector\n"
msgstr "%s Greška! Nije moguće duplicirati vektor niza\n"

msgid "%s Error! Unable to split %s with %c\n"
msgstr "%s Greška! Nije moguće podijeliti %s s %c\n"

msgid "%s-type"
msgstr "%s-tip"

msgid "%x"
msgstr "%x"

msgid "(Entry)"
msgstr "(Ulazak)"

msgid "(Repeat)"
msgstr "(Ponoviti)"

msgid "(Type)"
msgstr "(Tip)"

msgid ","
msgstr ","

msgid "-research mode with RegEx"
msgstr "-način istraživanja s RegEx"

msgid "-research mode with jokers(DOS like)"
msgstr "-način istraživanja s jokerima(DOS Kao)"

msgid ".\n"
msgstr ".\n"

msgid "0%"
msgstr "0%"

msgid "100%"
msgstr "100%"

msgid "<Copy and paste your sample text here>"
msgstr "<Ovdje kopirajte i zalijepite svoj uzorak teksta>"

msgid ""
"<b>Error!</b>\n"
"\n"
"Dates mismatch ! 'Before than' date must be <i>more recent</i> than 'After than' date.\n"
"<b>Search can't proceed correctly !</b>\n"
"Please check the dates."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Nepodudaranje datuma! Datum 'prije nego' mora biti <i>nedavno</i> od datuma 'Nakon'.\n"
"<b>Pretraga se ne može pravilno nastaviti!</b>\n"
"Molimo provjerite datume."

msgid ""
"<b>Error!</b>\n"
"\n"
"Invalid 'After'date - format as dd/mm/yyyy or dd mmm yy."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Nevažeći »Nakon« datum - formatirajte kao dd/mm/yyyy ilidd mmm yy."

msgid ""
"<b>Error!</b>\n"
"\n"
"Invalid 'Before' date - format as dd/mm/yyyy or dd mmm yy."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Nevažeći datum »prije« - formatirajte kao dd/mm/yyyy ili dd mmm yy."

msgid ""
"<b>Error!</b>\n"
"\n"
"LessThan file size must be <i>positive</i> value\n"
"So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Veličina datoteke mora biti manja od <i>pozitivan</i> vrijednost\n"
"Stoga se ovaj kriterij neće koristiti.\n"
"\n"
"Molimo provjerite svoj unos i jedinice."

msgid ""
"<b>Error!</b>\n"
"\n"
"MoreThan file size must be <i>positive</i> value\n"
"So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Mora biti veća od veličine datoteke <i>pozitivan</i> vrijednost\n"
"Stoga se ovaj kriterij neće koristiti.\n"
"\n"
"Molimo provjerite svoj unos i jedinice."

msgid ""
"<b>Error!</b>\n"
"\n"
"MoreThan file size must be <i>stricly inferior</i> to LessThan file size.So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Greška!</b>\n"
"\n"
"Mora biti veća od veličine datoteke <i>strogo inferioran</i> na veličinu datoteke LessThan. Stoga se ovaj kriterij neće koristiti.\n"
"\n"
"Molimo provjerite svoj unos i jedinice."

msgid "<b>Export Source</b>"
msgstr "<b>Izvor izvoza</b>"

msgid "<b>Export Type</b>"
msgstr "<b>Vrsta izvoza</b>"

msgid "<b>File Contents Options</b>"
msgstr "<b>Opcije sadržaja datoteke</b>"

msgid "<b>File Name Options</b>"
msgstr "<b>Opcije naziva datoteke</b>"

msgid "<b>Import Location</b>"
msgstr "<b>Mjesto uvoza</b>"

msgid "<b>Import Type</b>"
msgstr "<b>Vrsta uvoza</b>"

msgid "<b>Resulting expression</b>"
msgstr "<b>Rezultirajući izraz</b>"

msgid "<b>Results Options</b>"
msgstr "<b>Opcije rezultata</b>"

msgid "<b>Sample Text:</b>"
msgstr "<b>Primjer teksta:</b>"

msgid ""
"<b>Select which data to delete:</b>\n"
"With this dialog, you can control how your entries will be cleared."
msgstr ""
"<b>Odaberite koje podatke želite izbrisati:</b>\n"
"Pomoću ovog dijaloškog okvira možete kontrolirati kako će se vaši unosi brisati."

msgid "<b>Status:</b>"
msgstr "<b>Status:</b>"

msgid "<b>Test Expression</b>:"
msgstr "<b>Testni izraz</b>:"

msgid ""
"<b>Test Regular Expressions (RegEx):</b>\n"
"<i>With this dialog, you can test a RegEx before using it.\n"
"Feel free to type a RegEx and put an example of text to test it.</i>"
msgstr ""
"<b>Testirajte regularne izraze (RegEx):</b>\n"
"<i>S ovim dijaloškim okvirom možete testirati RegEx prije upotrebe.\n"
"Slobodno upišite RegEx i stavite primjer teksta da biste ga testirali.</i>"

msgid "<b>Text begins:</b>"
msgstr "<b>Tekst počinje:</b>"

msgid "<b>Text contains</b>"
msgstr "<b>Tekst sadrži</b>"

msgid "<b>Text ends:</b>"
msgstr "<b>Tekst završava:</b>"

msgid "<b>Warning</b>: Once cleared, history cannot be recovered!"
msgstr "<b>Upozorenje</b>: Nakon brisanja, povijest se ne može vratiti!"

msgid ""
"<i>Regular expression power search utility written in GTK+ and licensed "
"under GPL v.3</i>"
msgstr ""
"<i>Uslužni program za traženje snage regularnog izraza napisan GTK+ i "
"licenciran pod GPL v.3</i>"

msgid ""
"<span face=\"Arial\" foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\" foreground=\"#FF6600\"><b>Ex</b></span> Expression Builder..."
msgstr ""
"<span face=\"Arial\" foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\" foreground=\"#FF6600\"><b>Ex</b></span> Graditelj izraza..."

msgid ""
"<span face=\"Arial\"foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\"foreground=\"#FF6600\"><b>Ex</b></span> Expression Builder..."
msgstr ""
"<span face=\"Arial\"foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\"foreground=\"#FF6600\"><b>Ex</b></span> Graditelj izraza..."

msgid "A file named \"%s\" already exists. Are you sure you want to overwrite it?"
msgstr ""
"Datoteka pod nazivom \"%s\" već postoji. Jeste li sigurni da ga želite "
"prebrisati?"

msgid "Aborting research-Searchmonkey"
msgstr "Prekid istraživanja-Searchmonkey"

msgid "About searchmonkey"
msgstr "O searchmonkeyju"

msgid "About to overwrite save configuration. Do you want to proceed?"
msgstr "Sprema se zamjena konfiguracije spremanja. Želiš li nastaviti?"

msgid "Accept the resulting expression, and add it to the search rules."
msgstr "Prihvatite rezultirajući izraz i dodajte ga u pravila pretraživanja."

msgid ""
"Adam Cottrell (en_GB)\n"
"Luc Amimer (fr)"
msgstr ""
"Adam Cottrell (en_GB)\n"
"Luc Amimer (fr)\n"
"marcelocripe (hr)"

msgid "Add"
msgstr "Dodati"

msgid "Add search criteria from a text file. Similar to the grep -f flag."
msgstr ""
"Dodajte kriterije pretraživanja iz tekstualne datoteke. Slično kao grep -f "
"zastava."

msgid "Advanced"
msgstr "Napredna"

msgid "After:"
msgstr "Nakon:"

msgid "Any char except these"
msgstr "Bilo koji char osim ovih"

msgid "Any character"
msgstr "Bilo koji lik"

msgid "Any numeric character"
msgstr "Bilo koji brojčani znak"

msgid "Any one of these chars"
msgstr "Bilo koji od ovih likova"

msgid "Any text character"
msgstr "Bilo koji tekstualni znak"

msgid ""
"Are you sure that you wish to delete the config file, and restart "
"searchmonkey?"
msgstr ""
"Jeste li sigurni da želite izbrisati konfiguracijsku datoteku i ponovno "
"pokrenuti searchmonkey?"

msgid ""
"Are you sure you want to delete:\n"
"\t'%s'"
msgstr ""
"Jeste li sigurni da želite izbrisati:\n"
"\t'%s'"

msgid "Attributes"
msgstr "Atributi"

msgid "Auto Complete"
msgstr "Automatsko dovršavanje"

msgid "Auto search..."
msgstr "Automatsko traženje..."

msgid "Auto-size columns"
msgstr "Automatska veličina stupaca"

msgid "Basic"
msgstr "Osnovni, temeljni"

msgid "Before:"
msgstr "Prije:"

msgid "Binary/Executable"
msgstr "Binarno/izvršno"

msgid "Browse your file system and select the search directory..."
msgstr ""
"Pregledajte svoj datotečni sustav i odaberite direktorij za pretraživanje..."

msgid "C_ancel"
msgstr "C_ancel"

msgid "Cannot copy full file name as name was not selected."
msgstr "Nije moguće kopirati puni naziv datoteke jer naziv nije odabran."

msgid "Cannot delete file as name was not selected."
msgstr "Nije moguće izbrisati datoteku jer naziv nije odabran."

msgid "Check this to allow"
msgstr "Označite ovo da biste dopustili"

msgid "Clear History..."
msgstr "Očisti povijest..."

msgid "Clear Search History"
msgstr "Izbrišite povijest pretraživanja"

msgid "Clear all directories in the \"Folders:\" drop-down list"
msgstr "Obrišite sve direktorije u \"Mape:\" padajući popis"

msgid "Clear all file names in the \"Files:\" drop-down list"
msgstr "Izbrišite sve nazive datoteka u \"Datoteke:\" padajući popis"

msgid "Clear all patterns in the \"Containing:\" drop-down list"
msgstr "Obrišite sve uzorke u \"Sadrži:\" padajući popis"

msgid "Clear containing text"
msgstr "Izbriši koji sadrži tekst"

msgid "Clear file names"
msgstr "Izbrišite nazive datoteka"

msgid "Clear look in history"
msgstr "Jasan pogled u povijest"

msgid "Configuration"
msgstr "Konfiguracija"

msgid ""
"Configuration files are for a different version of Searchmonkey. This may "
"cause errors. Delete old configuration files?"
msgstr ""
"Konfiguracijske datoteke su za drugu verziju Searchmonkeyja. To može "
"uzrokovati pogreške. Izbrisati stare konfiguracijske datoteke?"

msgid "Containing Text Export Criteria"
msgstr "Sadrži kriterije za izvoz teksta"

msgid "Containing Text import"
msgstr "Sadrži uvoz teksta"

msgid "Containing:"
msgstr "koji sadrži:"

msgid "Context Expression Wizard"
msgstr "Čarobnjak za izražavanje konteksta"

msgid "Convert Internals"
msgstr "Pretvorite interne elemente"

msgid "Copyright (c) 2006-2018 Adam Cottrell"
msgstr "Copyright (c) 2006-2018 Adam Cottrell"

msgid "Couldn't find pixmap file: %s"
msgstr "Nije moguće pronaći pixmap datoteku: %s"

msgid "Create a CSV file containing a list of results."
msgstr "Izradite CSV datoteku koja sadrži popis rezultata."

msgid "Creates a new row in the list with the following rule."
msgstr "Stvara novi redak na popisu sa sljedećim pravilom."

msgid "Credits"
msgstr "Zasluge"

msgid "Csv Export"
msgstr "Csv izvoz"

msgid "Current Config file location:"
msgstr "Trenutna lokacija konfiguracijske datoteke:"

msgid "Default Applications"
msgstr "Zadane aplikacije"

msgid "Default folder explorer:"
msgstr "Zadani istraživač mapa:"

msgid "Default text editor:"
msgstr "Zadani uređivač teksta:"

msgid "Default web browser:"
msgstr "Zadani web preglednik:"

msgid "Delete"
msgstr "Izbrisati"

msgid "Deletes an existing row in the rule table."
msgstr "Briše postojeći redak u tablici pravila."

msgid "Desktop search to replace find and grep."
msgstr "Pretraživanje radne površine za zamjenu find i grep."

msgid "Did not match any."
msgstr "Ne odgovara nijednom."

msgid "Discard any changes."
msgstr "Odbacite sve promjene."

msgid "Displays the matching text lines when the result is selected."
msgstr "Prikazuje odgovarajuće retke teksta kada je odabran rezultat."

msgid "Don't know"
msgstr "ne znam"

msgid ""
"Each row creates a new part to the rule, with the order reflecting the order"
" in which the rule is to be matched."
msgstr ""
"Svaki red stvara novi dio pravila, s redoslijedom koji odražava redoslijed "
"kojim se pravilo treba upariti."

msgid "Edit (File)"
msgstr "Uredi (datoteka)"

msgid "Edit File"
msgstr "Uredi datoteku"

msgid "Enable contents search to find files with matching content too."
msgstr ""
"Omogućite pretraživanje sadržaja kako biste pronašli i datoteke s "
"odgovarajućim sadržajem."

msgid "End of row separator:"
msgstr "Kraj razdjelnika reda:"

msgid "Error saving %s: %s\n"
msgstr "Pogreška pri spremanju %s: %s\n"

msgid "Error! Invalid 'containing text' regular expression"
msgstr "Greška! Nevažeći regularni izraz »sadrži tekst«."

msgid "Error! Invalid 'file name' regular expression"
msgstr "Greška! Nevažeći regularni izraz »naziv datoteke«."

msgid "Error! Invalid Containing Text regular expression"
msgstr "Greška! Nevažeći regularni izraz koji sadrži tekst"

msgid "Error! Invalid File Name regular expression"
msgstr "Greška! Nevažeći regularni izraz naziva datoteke"

msgid "Error! Look In directory cannot be blank."
msgstr "Greška! Polje Traži u imeniku ne može biti prazno."

msgid "Error! Look In directory is invalid."
msgstr "Greška! Look In directory je nevažeći."

msgid "Error! No valid file selected."
msgstr "Greška! Nije odabrana nijedna važeća datoteka."

msgid "Error! Unable to find first combo entry. Is drop down list blank?"
msgstr ""
"Greška! Nije moguće pronaći prvi kombinirani unos. Je li padajući popis "
"prazan?"

msgid "Error! Unable to remove config file %s.\n"
msgstr "Greška! Nije moguće ukloniti konfiguracijsku datoteku %s.\n"

msgid ""
"Error! You must select at least one entry in order to save drop down list."
msgstr ""
"Greška! Morate odabrati barem jedan unos kako biste spremili padajući popis."

msgid "Expert Mode"
msgstr "Stručna razina"

msgid "Export Criteria"
msgstr "Izvozni kriteriji"

msgid "Export Expression..."
msgstr "Izvozni izraz..."

msgid "Expression Wizard"
msgstr "Čarobnjak za izraze"

msgid ""
"Extended regular expressions are recommended as they provide more "
"functionality."
msgstr ""
"Preporučuju se prošireni regularni izrazi jer pružaju više funkcionalnosti."

msgid "Extras"
msgstr "Dodaci"

msgid "File Expression Wizard"
msgstr "Čarobnjak za izražavanje datoteka"

msgid "File Name Export criteria"
msgstr "Naziv datoteke Kriteriji za izvoz"

msgid "File Name import"
msgstr "Uvoz naziva datoteke"

msgid "File Selection"
msgstr "Odabir datoteke"

msgid "File explorer attributes: %f=filename, %d=directory"
msgstr "Atributi File Explorera: %f=naziv datoteke, %d=imenik"

msgid "File name"
msgstr "Naziv datoteke"

msgid "File size"
msgstr "Veličina datoteke"

msgid "File type"
msgstr "Vrsta datoteke"

msgid "File was empty/invalid."
msgstr "Datoteka je bila prazna/nevažeća."

msgid "Files:"
msgstr "Datoteke:"

msgid "Find default executables:"
msgstr "Pronađite zadane izvršne datoteke:"

msgid "Folders:"
msgstr "Mape:"

msgid "Follow Symbol _Links"
msgstr "Slijedite Simbol _Linkovi"

msgid "Follow Symbolic Links (Only on systems that support them)"
msgstr "Slijedite simboličke veze (samo na sustavima koji ih podržavaju)"

msgid "Force a prompt before any files are deleted"
msgstr "Forsirajte upit prije brisanja bilo koje datoteke"

msgid "Force a prompt before settings are saved."
msgstr "Forsirajte upit prije spremanja postavki."

msgid "Force single window mode"
msgstr "Prisilni način rada s jednim prozorom"

msgid "Found %d matches."
msgstr "Pronađeno %d šibice."

msgid "Found 1 match."
msgstr "Pronađeno 1 podudaranje."

msgid "Gb"
msgstr "Gb"

msgid "Global Settings"
msgstr "Globalne postavke"

msgid "Grep style file (1-entry, OR each line)"
msgstr "Grep stilska datoteka (1-unos, ILI svaki redak)"

msgid "Help with the Regular Expression Builder"
msgstr "Pomoć s alatom za izgradnju regularnih izraza"

msgid "Highlights per line :"
msgstr "Istaknuto po retku:"

msgid "Horizontal results"
msgstr "Horizontalni rezultati"

msgid ""
"If active, display extra lines around the matching line to help show context"
" of match."
msgstr ""
"Ako je aktivan, prikažite dodatne linije oko odgovarajućeg retka kako biste "
"lakše prikazali kontekst podudaranja."

msgid "Ignore hidden files (Unix filesystems only)"
msgstr "Zanemari skrivene datoteke (samo Unix datotečni sustavi)"

msgid ""
"Illegal escape sequence: '\\%c'.\n"
" Valid escape characters are: b n r t \" \\"
msgstr ""
"Nezakonit bijeg niz: '\\%c'.\n"
" Važeći escape znakovi su: b n r t \" \\"

msgid "Import Criteria"
msgstr "Uvozni kriteriji"

msgid "Import Expression..."
msgstr "Uvozni izraz..."

msgid "Import a file identical to that generated by the export command."
msgstr "Uvezite datoteku identičnu onoj koju je generirala naredba za izvoz."

msgid ""
"Input string expands into non ASCII, or multi-byte key sequence: '%s'.\n"
" Please try to use ASCII safe keys"
msgstr ""
"Ulazni niz se proširuje u ne ASCII ili višebajtni niz ključeva: '%s'.\n"
" Pokušajte koristiti ASCII sigurne ključeve"

msgid "Internal Error! Unable to find calling wizard type!"
msgstr "Interna pogreška! Nije moguće pronaći vrstu čarobnjaka za pozivanje!"

msgid "Invert Search Expression"
msgstr "Invertiranje izraza pretraživanja"

msgid "Inverts the file name search results i.e. find all becomes find none."
msgstr ""
"Invertira rezultate pretraživanja naziva datoteke, tj. pronađi sve postaje "
"pronaći ništa."

msgid "Just save the currently displayed entry to a file."
msgstr "Samo spremite trenutno prikazani unos u datoteku."

msgid "Kb"
msgstr "Kb"

msgid "Less than:"
msgstr "Manje od:"

msgid "Limit Content Highlighting"
msgstr "Ograničite isticanje sadržaja"

msgid "Limit Match Files"
msgstr "Ograničite datoteke podudaranja"

msgid "Limit files found by selecting maximum tree-depth to search."
msgstr ""
"Ograničite pronađene datoteke odabirom maksimalne dubine stabla za "
"pretraživanje."

msgid "Line Number: %d\n"
msgstr "Broj retka: %d\n"

msgid "Lines Displayed:"
msgstr "Prikazane linije:"

msgid "List of file(s) corresponding to research criteria."
msgstr "Popis datoteka koje odgovaraju kriterijima istraživanja."

msgid "Location"
msgstr "Mjesto"

msgid "Location of your current searchmonkey configuration settings file."
msgstr "Lokacija vaše trenutne datoteke postavki konfiguracije searchmonkey."

msgid "Mat_ches"
msgstr "Mat_ches"

msgid "Match Case"
msgstr "Kutija šibica"

msgid "Match strings within binary files too"
msgstr "Uskladite nizove i unutar binarnih datoteka"

msgid "Matches"
msgstr "Utakmice"

msgid "Maximum Hits:"
msgstr "Maksimalni broj pogodaka:"

msgid "Maximum filesize in kBytes/mBytes/gBytes."
msgstr "Maksimalna veličina datoteke u kBytes/mBytes/gBytes."

msgid "Mb"
msgstr "Mb"

msgid "Minimum filesize in kBytes/mBytes/gBytes."
msgstr "Minimalna veličina datoteke u kBytes/mBytes/gBytes."

msgid "Modified"
msgstr "Izmijenjeno"

msgid "Modified date"
msgstr "Datum izmjene"

msgid "Modified:"
msgstr "Izmijenjeno:"

msgid "Modifies an existing row in the rule table."
msgstr "Mijenja postojeći redak u tablici pravila."

msgid "Modify"
msgstr "Izmijeniti"

msgid "More than:"
msgstr "Više od:"

msgid ""
"Multi-byte string entered: '%s'.\n"
"Truncating extra character (%c)"
msgstr ""
"Uneseni višebajtni niz: '%s'.\n"
"Skraćivanje dodatnog znaka (%c)"

msgid "N/A"
msgstr "N/A"

msgid "Name"
msgstr "Ime"

msgid "New Instance"
msgstr "Nova instanca"

msgid "No results to save!"
msgstr "Nema rezultata za spremanje!"

msgid "Once"
msgstr "Jednom"

msgid "One or more times"
msgstr "Jednom ili više puta"

msgid "Open Folder"
msgstr "Otvorite mapu"

msgid "Open a new search window."
msgstr "Otvorite novi prozor za pretraživanje."

msgid "Open calendar dialog to choose modified date."
msgstr "Otvorite dijaloški okvir kalendara da odaberete izmijenjeni datum."

msgid "Opens a dialog to find a look in directory."
msgstr "Otvara dijaloški okvir za traženje pogleda u imeniku."

msgid "Options"
msgstr "Mogućnosti"

msgid "Options: "
msgstr "Opcije:"

msgid "P_rint Setup..."
msgstr "Postavljanje P_printa..."

msgid "Paragraph Number: %d\n"
msgstr "Broj stavka: %d\n"

msgid "Paste and edit full file name of your preferred file explorer."
msgstr ""
"Zalijepite i uredite puni naziv datoteke željenog istraživača datoteka."

msgid "Paste and edit full file name of your preferred text editor."
msgstr "Zalijepite i uredite puni naziv datoteke željenog uređivača teksta."

msgid "Paste and edit full file name of your preferred web browser."
msgstr "Zalijepite i uredite puni naziv datoteke željenog web preglednika."

msgid ""
"Perform content searching immediately so that results are returned quicker."
msgstr ""
"Odmah izvršite pretraživanje sadržaja kako bi se rezultati brže vratili."

msgid "Phase 1 searching %s"
msgstr "Faza 1 pretraživanja %s"

msgid "Phase 2 searching %s"
msgstr "Faza 2 pretraživanja %s"

msgid "Phase 2 starting..."
msgstr "Počinje 2. faza..."

msgid ""
"Please be patient!\n"
"\n"
"Searching for executables to perform the following functions:\n"
"  * text editing/viewing\n"
"  * folder browsing\n"
"  * web browsing\n"
"\n"
"The search algorithm uses a set of standard executables for all systems, and then attempts to find them on your local disk.\n"
"\n"
"Once complete, press OK to store the executables."
msgstr ""
"Molimo budite strpljivi!\n"
"\n"
"Traženje izvršnih datoteka za obavljanje sljedećih funkcija:\n"
"  * uređivanje/pregled teksta\n"
"  * pregledavanje mapa\n"
"  * pregledavanje weba\n"
"\n"
"Algoritam pretraživanja koristi skup standardnih izvršnih datoteka za sve sustave, a zatim ih pokušava pronaći na vašem lokalnom disku.\n"
"\n"
"Kada završite, pritisnite OK za spremanje izvršnih datoteka."

msgid "Please email all debug text to cottrela@users.sf.net.\n"
msgstr ""
"Pošaljite e-poruku za sav tekst za otklanjanje pogrešaka "
"cottrela@users.sf.net.\n"

msgid "Please wait..."
msgstr "Molimo pričekajte..."

msgid ""
"Please, type here a word or a segment of a word researched in the file(s).\n"
"Don't type spaces."
msgstr ""
"Molimo, upišite ovdje riječ ili dio riječi istražene u datoteci(ama).\n"
"Nemojte upisivati razmake."

msgid ""
"Please, type here the name or a segment of the name of the file(s).\n"
"Don't type spaces."
msgstr ""
"Molimo vas da ovdje upišete naziv ili dio naziva datoteke(a).\n"
"Nemojte upisivati razmake."

msgid "Possibly once"
msgstr "Moguće jednom"

msgid "Print results direct to the configured printer."
msgstr "Ispišite rezultate izravno na konfigurirani pisač."

msgid ""
"Project Manager:\n"
"Adam Cottrell <cottrela@users.sf.net>\n"
"\n"
"Programming:\n"
"  Adam Cottrell <cottrela@users.sf.net>\n"
"  Salil Joshi <HellFeuer@users.sf.net>\n"
"  Yousef AlHashemi\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
"Artwork:\n"
"  Peter Cruickshank\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
msgstr ""
"Voditelj projekta:\n"
"Adam Cottrell <cottrela@users.sf.net>\n"
"\n"
"Programiranje:\n"
"  Adam Cottrell <cottrela@users.sf.net>\n"
"  Salil Joshi <HellFeuer@users.sf.net>\n"
"  Yousef AlHashemi\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
"Ilustracije:\n"
"  Peter Cruickshank\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"

msgid "Prompt before deleting files"
msgstr "Pitajte prije brisanja datoteka"

msgid "Prompt before saving state"
msgstr "Uvjeti prije spremanja stanja"

msgid "Ready-research mode with RegEx"
msgstr "Spreman način istraživanja s RegEx-om"

msgid "Ready-research mode with jokers(DOS like)"
msgstr "Spremni način istraživanja s šaljivcima(DOS Kao)"

msgid "Recurse Folders"
msgstr "Rekurzivne mape"

msgid "Release _Notes"
msgstr "Otpustite _Bilješke"

msgid "Remove all saved settings (restart required to complete):"
msgstr ""
"Uklonite sve spremljene postavke (za dovršetak je potrebno ponovno "
"pokretanje):"

msgid "Reset all"
msgstr "Resetiraj sve"

msgid "Reset size/modified options"
msgstr "Poništi veličinu/izmijenjene opcije"

msgid "Reset the size and modified search criteria."
msgstr "Poništite veličinu i izmijenjene kriterije pretraživanja."

msgid "Restrict depth:"
msgstr "Ograničenje dubine:"

msgid ""
"Run configuration to set default directory/folder browser:\n"
"\"%s\""
msgstr ""
"Pokrenite konfiguraciju za postavljanje zadanog preglednika direktorija/mape:\n"
"\"%s\""

msgid ""
"Run configuration to set default text editor:\n"
"\"%s\""
msgstr ""
"Pokrenite konfiguraciju za postavljanje zadanog uređivača teksta:\n"
"\"%s\""

msgid ""
"Run configuration to set default web-browser:\n"
"\"%s\""
msgstr ""
"Pokrenite konfiguraciju za postavljanje zadanog web-preglednika:\n"
"\"%s\""

msgid "S_ort by"
msgstr "S_ort po"

msgid "Save R_esults to CSV..."
msgstr "Uštedjeti R_esults do CSV..."

msgid "Save Results..."
msgstr "Spremi rezultate..."

msgid "Save active/displayed entry only"
msgstr "Spremi samo aktivni/prikazani unos"

msgid "Save all entries (1-entry per line)"
msgstr "Spremi sve unose (1 unos po retku)"

msgid "Save now"
msgstr "Spremi sada"

msgid "Save results to CSV file..."
msgstr "Spremi rezultate u CSV datoteku..."

msgid "Save settings immediately (normally occurs on exit):"
msgstr "Odmah spremite postavke (obično se događa na izlazu):"

msgid "Save the Containing Text drop down entries into a file."
msgstr "Spremite padajuće unose Containing Text u datoteku."

msgid "Save the File Name drop down entries into a file."
msgstr "Spremite padajuće unose naziva datoteke u datoteku."

msgid "Search criteria"
msgstr "Kriterij pretrage"

msgid "Search progress"
msgstr "Napredak pretraživanja"

msgid "Search progress bar."
msgstr "Traka napretka pretraživanja."

msgid "Searching for system files, please wait..."
msgstr "Traženje sistemskih datoteka, pričekajte..."

msgid "Searchmonkey : search in %s/"
msgstr "Searchmonkey : traži u %s/"

msgid "Searchmonkey style (1-entry per line)"
msgstr "Searchmonkey stil (1 unos po retku)"

msgid "Select Date..."
msgstr "Odaberite Datum..."

msgid "Select Font"
msgstr "Odaberite Font"

msgid "Select a folder"
msgstr "Odaberite mapu"

msgid "Select highlighting Colour"
msgstr "Odaberite Boja isticanja"

msgid "Settings"
msgstr "Postavke"

msgid "Show Line Contents"
msgstr "Prikaži sadržaj retka"

msgid "Show _Statusbar"
msgstr "Pokazati _Statusna traka"

msgid "Show _Toolbar"
msgstr "Pokazati _Alatna traka"

msgid "Size"
msgstr "Veličina"

msgid "Size :"
msgstr "veličina:"

msgid ""
"Some characters (such as . | ^ $) are reserved in certain circumstances.\n"
"\n"
"If you know what you are doing with these characters, unselect this checkbox."
msgstr ""
"Neki likovi (kao npr . | ^ $) rezervirani su u određenim okolnostima.\n"
"\n"
"Ako znate što radite s tim znakovima, poništite odabir ovog potvrdnog okvira."

msgid "Space(s)"
msgstr "prostor(i)"

msgid ""
"Specify the maximum number of content matches within each file to show. Does"
" not limit the number of files to search."
msgstr ""
"Odredite maksimalni broj podudaranja sadržaja unutar svake datoteke za "
"prikaz. Ne ograničava broj datoteka za pretraživanje."

msgid ""
"Specify the maximum number of matching files found. Does not limit the "
"number of content matches within each file."
msgstr ""
"Navedite maksimalni broj pronađenih podudarnih datoteka. Ne ograničava broj "
"podudaranja sadržaja unutar svake datoteke."

msgid "Start"
msgstr "Početak"

msgid "Start the search!"
msgstr "Započnite pretragu!"

msgid "Stop"
msgstr "Stop"

msgid "Stop the search!"
msgstr "Zaustavite potragu!"

msgid "Switch search from basic to advanced mode."
msgstr "Prebacite pretraživanje iz osnovnog u napredni način."

msgid "System"
msgstr "Sustav"

msgid "Test Regular Expression"
msgstr "Testirajte regularni izraz"

msgid "Test Regular Expression..."
msgstr "Testirajte regularni izraz..."

msgid "Text delimiter:"
msgstr "Razdjelnik teksta:"

msgid "Text editor attributes: %f=filename, %d=directory"
msgstr "Atributi uređivača teksta: %f=naziv datoteke, %d=imenik"

msgid "The character"
msgstr "Lik"

msgid "The phrase"
msgstr "Fraza"

msgid "This is the expression that will be copied back to the main screen."
msgstr "Ovo je izraz koji će se kopirati natrag na glavni zaslon."

msgid ""
"To see the results of your regular expression click apply. Once done, simply close the window. Searches are case sensitive.\n"
"\n"
"<b>Note:</b> this tool will highlight <i>each</i> match(es) found."
msgstr ""
"Da biste vidjeli rezultate svog regularnog izraza, kliknite Primijeni. Kada završite, jednostavno zatvorite prozor. Pretraživanja su osjetljiva na velika i mala slova.\n"
"\n"
"<b>Bilješka:</b> ovaj alat će istaknuti <i>svaki</i> pronađena podudaranja."

msgid "Translated by"
msgstr "Preveo"

msgid "Type"
msgstr "Tip"

msgid "Unknown"
msgstr "nepoznato"

msgid "Update"
msgstr "ažuriranje"

msgid "Updates a rule that was selected for the modify function."
msgstr "Ažurira pravilo koje je odabrano za funkciju izmjene."

msgid "Use ASCII characters e.g. \" # $ '"
msgstr "Koristite ASCII znakove, npr. \" # $ '"

msgid "Use ASCII or C style construct e.g. , \\t ;"
msgstr "Koristite ASCII ili C stilsku konstrukciju, npr. , \\t ;"

msgid "Use ASCII or C style construct e.g. \\n"
msgstr "Koristite ASCII ili C stilsku konstrukciju, npr. \\n"

msgid "Use Single Phase Search"
msgstr "Koristite jednofazno pretraživanje"

msgid "Use Wil_dcard Syntax"
msgstr "Koristiti Wil_dcard Sintaksa"

msgid "Use _Regular Expression Syntax"
msgstr "Koristiti _Sintaksa regularnog izraza"

msgid "Use _Word Wrap"
msgstr "Koristiti _Prelamanje riječi"

msgid "Use extended regular expressions"
msgstr "Koristite proširene regularne izraze"

msgid "Use regular expression syntax in file search criteria."
msgstr ""
"Koristite sintaksu regularnog izraza u kriterijima pretraživanja datoteka."

msgid "Use simple search syntax"
msgstr "Koristite jednostavnu sintaksu pretraživanja"

msgid "Use strict contents case matching."
msgstr "Koristite strogo podudaranje sadržaja velikih i malih slova."

msgid "Use strict file case matching."
msgstr "Koristite strogo podudaranje velikih i malih slova."

msgid "Use the file in an identical to way to that of the grep -f command."
msgstr "Koristite datoteku na identičan način kao grep -f naredba."

msgid "Use the wizard to create a containing text regular expression..."
msgstr ""
"Koristite čarobnjak za stvaranje regularnog izraza koji sadrži tekst..."

msgid "Use the wizard to create a file name regular expression..."
msgstr "Koristite čarobnjak za stvaranje regularnog izraza naziva datoteke..."

msgid "Use this option to choose the file size units."
msgstr "Koristite ovu opciju za odabir jedinica veličine datoteke."

msgid ""
"Use this option to restrict your search results to files containing the "
"provided text (or pattern)."
msgstr ""
"Koristite ovu opciju za ograničavanje rezultata pretraživanja na datoteke "
"koje sadrže navedeni tekst (ili uzorak)."

msgid ""
"Use this option to restrict your search results to files that are bigger "
"than the provided number of kilo Bytes."
msgstr ""
"Koristite ovu opciju da ograničite rezultate pretraživanja na datoteke koje "
"su veće od navedenog broja kilobajtova."

msgid ""
"Use this option to restrict your search results to files that are smaller "
"than the provided number of kilo Bytes."
msgstr ""
"Koristite ovu opciju da ograničite rezultate pretraživanja na datoteke koje "
"su manje od navedenog broja kilobajtova."

msgid ""
"Use this option to restrict your search results to files that were last "
"modified after the provided time."
msgstr ""
"Koristite ovu opciju da ograničite rezultate pretraživanja na datoteke koje "
"su zadnji put izmijenjene nakon navedenog vremena."

msgid ""
"Use this option to restrict your search results to files that were last "
"modified before the provided time."
msgstr ""
"Koristite ovu opciju da ograničite rezultate pretraživanja na datoteke koje "
"su zadnji put izmijenjene prije navedenog vremena."

msgid "Use this option to search all subfolders (recursively)."
msgstr "Koristite ovu opciju za pretraživanje svih podmapa (rekurzivno)."

msgid "Use wildcard or glob syntax for file search criteria. E.g. *.txt"
msgstr ""
"Koristite zamjenski znak ili sintaksu globa za kriterije pretraživanja "
"datoteka. Npr. *.txt"

msgid "Value/field separator:"
msgstr "Razdjelnik vrijednosti/polja:"

msgid "Vertical results"
msgstr "Vertikalni rezultati"

msgid "Write some or all of the search criteria to disk."
msgstr "Zapišite neke ili sve kriterije pretraživanja na disk."

msgid "Written by"
msgstr "Napisao"

msgid "Zero or more times"
msgstr "Nula ili više puta"

msgid "\\\""
msgstr "\\\""

msgid "\\n"
msgstr "\\n"

msgid "_Containing:"
msgstr "_koji sadrži:"

msgid "_Copy (Filename)"
msgstr "_Kopiraj (naziv datoteke)"

msgid "_Delete (File)"
msgstr "_Izbrisati dateoteku)"

msgid "_Delete File"
msgstr "_Izbrisati dateoteku"

msgid "_Edit"
msgstr "_Uredi"

msgid "_Explore Folder"
msgstr "_Istražite mapu"

msgid "_Export Regular Expression..."
msgstr "_Izvezi regularni izraz..."

msgid "_File"
msgstr "_Datoteka"

msgid "_File Name"
msgstr "_Naziv datoteke"

msgid "_Forums"
msgstr "_Forumi"

msgid "_Help"
msgstr "_Pomozite"

msgid "_Ignore files beginning with '.'"
msgstr "_Zanemarite datoteke koje počinju s '.'"

msgid "_Import Regular Expression..."
msgstr "_Uvezi regularni izraz..."

msgid "_Location"
msgstr "_Mjesto"

msgid "_Modified"
msgstr "_Izmijenjeno"

msgid "_New Instance"
msgstr "_Nova instanca"

msgid "_Search"
msgstr "_traži"

msgid "_Size"
msgstr "_Veličina"

msgid "_Support"
msgstr "_Podrška"

msgid "_Type"
msgstr "_Tip"

msgid "_User Guide"
msgstr "_Korisnički vodič"

msgid "_View"
msgstr "_Pogled"

msgid "case sensitive; "
msgstr "osjetljivo na velika i mala slova; "

msgid "display %d extra lines around match; "
msgstr "prikaz %d dodatne linije oko utakmice; "

msgid "display 1 extra line around match; "
msgstr "prikazati 1 dodatni red oko utakmice; "

msgid "http://searchmonkey.embeddediq.com/index.php/contribute"
msgstr "http://searchmonkey.embeddediq.com/index.php/contribute"

msgid "http://searchmonkey.embeddediq.com/index.php/support"
msgstr "http://searchmonkey.embeddediq.com/index.php/support"

msgid "http://searchmonkey.embeddediq.com/index.php/support/index"
msgstr "http://searchmonkey.embeddediq.com/index.php/support/index"

msgid ""
"http://searchmonkey.sourceforge.net/index.php/Regular_expression_builder"
msgstr ""
"http://searchmonkey.sourceforge.net/index.php/Regular_expression_builder"

msgid "http://sourceforge.net/forum/?group_id=175143"
msgstr "http://sourceforge.net/forum/?group_id=175143"

msgid ""
"https://sourceforge.net/projects/searchmonkey/files/gSearchmonkey%20GTK%20%28Gnome%29/0.8.2%20%5Bstable%5D/"
msgstr ""
"https://sourceforge.net/projects/searchmonkey/files/gSearchmonkey%20GTK%20%28Gnome%29/0.8.2%20%5Bstable%5D/"

msgid "none"
msgstr "nijedan"

msgid "only showing first %d content matches; "
msgstr "prikazuje samo prvo %d podudaranja sadržaja; "

msgid "only showing first content match; "
msgstr "prikazuje samo prvo podudaranje sadržaja; "

msgid "only showing line numbers; "
msgstr "prikazuje samo brojeve redaka; "

msgid "research mode with RegEx"
msgstr "način istraživanja s RegEx-om"

msgid "research mode with jokers(DOS like)"
msgstr "način istraživanja s džokerima (DOS like)"

msgid "sample"
msgstr "uzorak"

msgid "searchmonkey"
msgstr "trazimajmun"

msgid "searchmonkey %s"
msgstr "searchmonkey %s"

msgid "searchmonkey Credits"
msgstr "searchmonkey krediti"

msgid "that occurs"
msgstr "koji se događa"
