# 
# Translators:
# marcelo cripe <marcelocripe@gmail.com>, 2022
# 
msgid ""
msgstr ""
"Project-Id-Version: 0.8.1\n"
"Report-Msgid-Bugs-To: forum-antixlinux.com\n"
"POT-Creation-Date: 2022-01-26 17:42+0100\n"
"Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022\n"
"Language-Team: Nyanja (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ny/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ny\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.3\n"

msgid ""
"\n"
"Internal error %d! Unable to highlight line - offset beyond line-end.\n"
msgstr ""
"\n"
"Cholakwika chamkati %d! Sitingathe kuunikira mzere - kuchepetsa kupitirira kumapeto kwa mzere.\n"

msgid ""
"\n"
"No content searching performed for this file.\n"
"\n"
"Please ensure containing search was enabled, and that an expression was entered prior to starting search.\n"
msgstr ""
"\n"
"Palibe kusaka komwe kwachitika pafayiloyi.\n"
"\n"
"Chonde onetsetsani kuti muli ndi kusaka kwayatsidwa, komanso kuti mawu adalembedwa musanayambe kusaka.\n"

msgid "  Debug: %d) '%s'\n"
msgstr "  Chotsani: %d) '%s'\n"

msgid "  Debug: fn='%s'\n"
msgstr "  Chotsani: fn='%s'\n"

msgid "  Debug: tc=%d + os=%d || oe=%d\n"
msgstr "  Chotsani: tc=%d + os=%d || oe=%d\n"

msgid " %s settings auto-generated - Do not edit!"
msgstr " %s makonda opangidwa okha - Osasintha!"

msgid " [cancelled]"
msgstr " [kuthetsedwa]"

msgid " [inv]"
msgstr " [inv]"

msgid " [missing]"
msgstr " [akusowa]"

msgid "%1.1f KB"
msgstr "%1.1f KB"

msgid "%1.1f MB"
msgstr "%1.1f MB"

msgid "%d"
msgstr "%d"

msgid "%d bytes"
msgstr "%d mabayiti"

msgid "%d file found"
msgstr "%d fayilo yapezeka"

msgid "%d files found"
msgstr "%d mafayilo apezeka"

msgid "%f"
msgstr "%f"

msgid "%s %s\n"
msgstr "%s %s\n"

msgid "%s Error! Unable to duplicate string vector\n"
msgstr "%s Zolakwika! Takanika kubwereza vekitala ya zingwe\n"

msgid "%s Error! Unable to split %s with %c\n"
msgstr "%s Zolakwika! Sitingathe kugawanika %s ndi %c\n"

msgid "%s-type"
msgstr "%s-mtundu"

msgid "%x"
msgstr "%x"

msgid "(Entry)"
msgstr "(Kulowa)"

msgid "(Repeat)"
msgstr "(Bwerezani)"

msgid "(Type)"
msgstr "(Mtundu)"

msgid ","
msgstr ","

msgid "-research mode with RegEx"
msgstr "-kafukufuku mode ndi RegEx"

msgid "-research mode with jokers(DOS like)"
msgstr "-kafukufuku mode ndi nthabwala(DOS monga)"

msgid ".\n"
msgstr ".\n"

msgid "0%"
msgstr "0%"

msgid "100%"
msgstr "100%"

msgid "<Copy and paste your sample text here>"
msgstr "<Koperani ndi kumata chitsanzo chanu apa>"

msgid ""
"<b>Error!</b>\n"
"\n"
"Dates mismatch ! 'Before than' date must be <i>more recent</i> than 'After than' date.\n"
"<b>Search can't proceed correctly !</b>\n"
"Please check the dates."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"Madeti akusiyana! 'Isanafike' tsiku liyenera kukhala <i>zaposachedwa</i> kuposa 'Pambuyo pa' tsiku.\n"
"<b>Kusaka sikungapitirire bwino!</b>\n"
"Chonde onani madeti."

msgid ""
"<b>Error!</b>\n"
"\n"
"Invalid 'After'date - format as dd/mm/yyyy or dd mmm yy."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"'After'date' yosavomerezeka - ikani ngati dd/mm/yyyy kapenadd mmm yy."

msgid ""
"<b>Error!</b>\n"
"\n"
"Invalid 'Before' date - format as dd/mm/yyyy or dd mmm yy."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"Tsiku la 'Isanakwane' ndi losalondola - sinthani ngati dd/mm/yyyy kapena dd mmm yy."

msgid ""
"<b>Error!</b>\n"
"\n"
"LessThan file size must be <i>positive</i> value\n"
"So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"LessThan wapamwamba kukula ayenera kukhala <i>zabwino</i> mtengo\n"
"Kotero izi sizidzagwiritsidwa ntchito.\n"
"\n"
"Chonde, onani zomwe mwalemba komanso mayunitsi."

msgid ""
"<b>Error!</b>\n"
"\n"
"MoreThan file size must be <i>positive</i> value\n"
"So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"Kuposa kukula kwa fayilo kuyenera kukhala <i>zabwino</i> mtengo\n"
"Kotero izi sizidzagwiritsidwa ntchito.\n"
"\n"
"Chonde, onani zomwe mwalemba komanso mayunitsi."

msgid ""
"<b>Error!</b>\n"
"\n"
"MoreThan file size must be <i>stricly inferior</i> to LessThan file size.So this criteria will not be used.\n"
"\n"
"Please, check your entry and the units."
msgstr ""
"<b>Zolakwika!</b>\n"
"\n"
"Kuposa kukula kwa fayilo kuyenera kukhala <i>kutsika kwenikweni</i> to LessThan file size.Choncho izi sizidzagwiritsidwa ntchito.\n"
"\n"
"Chonde, onani zomwe mwalemba komanso mayunitsi."

msgid "<b>Export Source</b>"
msgstr "<b>Kutumiza kunja Source</b>"

msgid "<b>Export Type</b>"
msgstr "<b>Mtundu Wotumiza kunja</b>"

msgid "<b>File Contents Options</b>"
msgstr "<b>Zosankha Zamkatimu Zafayilo</b>"

msgid "<b>File Name Options</b>"
msgstr "<b>Zosankha za Dzina la Fayilo</b>"

msgid "<b>Import Location</b>"
msgstr "<b>Lowetsani Malo</b>"

msgid "<b>Import Type</b>"
msgstr "<b>Mtundu Wolowetsa</b>"

msgid "<b>Resulting expression</b>"
msgstr "<b>Zotsatira zake</b>"

msgid "<b>Results Options</b>"
msgstr "<b>Zotsatira Zosankha</b>"

msgid "<b>Sample Text:</b>"
msgstr "<b>Malemba Achitsanzo:</b>"

msgid ""
"<b>Select which data to delete:</b>\n"
"With this dialog, you can control how your entries will be cleared."
msgstr ""
"<b>Sankhani deta yoti mufufute:</b>\n"
"Ndi dialog iyi, mutha kuwongolera momwe zolemba zanu zichotsedwere."

msgid "<b>Status:</b>"
msgstr "<b>Mkhalidwe:</b>"

msgid "<b>Test Expression</b>:"
msgstr "<b>Mawu Oyesa</b>:"

msgid ""
"<b>Test Regular Expressions (RegEx):</b>\n"
"<i>With this dialog, you can test a RegEx before using it.\n"
"Feel free to type a RegEx and put an example of text to test it.</i>"
msgstr ""
"<b>Yesani Mawu Okhazikika (RegEx):</b>\n"
"<i>Ndi dialog iyi, mutha kuyesa RegEx musanagwiritse ntchito.\n"
"Khalani omasuka kulemba RegEx ndikuyika chitsanzo cha mawu kuti muyese.</i>"

msgid "<b>Text begins:</b>"
msgstr "<b>Mawu akuyamba:</b>"

msgid "<b>Text contains</b>"
msgstr "<b>Zolemba zili ndi</b>"

msgid "<b>Text ends:</b>"
msgstr "<b>Mawu akutha:</b>"

msgid "<b>Warning</b>: Once cleared, history cannot be recovered!"
msgstr "<b>Chenjezo</b>: Ikachotsedwa, mbiri siingapezekenso!"

msgid ""
"<i>Regular expression power search utility written in GTK+ and licensed "
"under GPL v.3</i>"
msgstr ""
"<i>Chida chofufuzira champhamvu cholembedwa mokhazikika GTK+ ndi chilolezo "
"pansi GPL v.3</i>"

msgid ""
"<span face=\"Arial\" foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\" foreground=\"#FF6600\"><b>Ex</b></span> Expression Builder..."
msgstr ""
"<span face=\"Arial\" foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\" foreground=\"#FF6600\"><b>Ex</b></span> Mawu Omanga..."

msgid ""
"<span face=\"Arial\"foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\"foreground=\"#FF6600\"><b>Ex</b></span> Expression Builder..."
msgstr ""
"<span face=\"Arial\"foreground=\"#670099\"><b>Reg</b></span><span "
"face=\"Arial\"foreground=\"#FF6600\"><b>Ex</b></span> Mawu Omanga..."

msgid "A file named \"%s\" already exists. Are you sure you want to overwrite it?"
msgstr ""
"Fayilo yotchedwa \"%s\" alipo kale. Kodi mukutsimikiza kuti mukufuna "
"kuichotsa?"

msgid "Aborting research-Searchmonkey"
msgstr "Kuthetsa kafukufuku-Searchmonkey"

msgid "About searchmonkey"
msgstr "Za searchmonkey"

msgid "About to overwrite save configuration. Do you want to proceed?"
msgstr "Zatsala pang'ono kulemba sungani kasinthidwe. Mukufuna kupitiriza?"

msgid "Accept the resulting expression, and add it to the search rules."
msgstr "Landirani mawuwo, ndikuwonjezera ku malamulo osaka."

msgid ""
"Adam Cottrell (en_GB)\n"
"Luc Amimer (fr)"
msgstr ""
"Adam Cottrell (en_GB)\n"
"Luc Amimer (fr)\n"
"marcelocripe (ny)"

msgid "Add"
msgstr "Onjezani"

msgid "Add search criteria from a text file. Similar to the grep -f flag."
msgstr ""
"Onjezani njira zofufuzira kuchokera mufayilo yamawu. Zofanana ndi grep -f "
"mbendera."

msgid "Advanced"
msgstr "Zapamwamba"

msgid "After:"
msgstr "Pambuyo:"

msgid "Any char except these"
msgstr "Chilichonse kupatula izi"

msgid "Any character"
msgstr "Khalidwe lililonse"

msgid "Any numeric character"
msgstr "Nambala iliyonse"

msgid "Any one of these chars"
msgstr "Iliyonse mwa zilembo izi"

msgid "Any text character"
msgstr "Mawu aliwonse"

msgid ""
"Are you sure that you wish to delete the config file, and restart "
"searchmonkey?"
msgstr ""
"Mukutsimikiza kuti mukufuna kuchotsa fayilo ya config, ndikuyambitsanso "
"searchmonkey?"

msgid ""
"Are you sure you want to delete:\n"
"\t'%s'"
msgstr ""
"Mukutsimikiza kuti mukufuna kuchotsa:\n"
"\t'%s'"

msgid "Attributes"
msgstr "Makhalidwe"

msgid "Auto Complete"
msgstr "Auto Complete"

msgid "Auto search..."
msgstr "Kusaka zokha..."

msgid "Auto-size columns"
msgstr "Zigawo zazikuluzikulu zokha"

msgid "Basic"
msgstr "Basic"

msgid "Before:"
msgstr "M'mbuyomu:"

msgid "Binary/Executable"
msgstr "Binary/Executable"

msgid "Browse your file system and select the search directory..."
msgstr "Sakatulani fayilo yanu ndikusankha chikwatu chosakira..."

msgid "C_ancel"
msgstr "C_ancel"

msgid "Cannot copy full file name as name was not selected."
msgstr ""
"Sitingathe kukopera dzina lafayilo yonse popeza dzina silinasankhidwe."

msgid "Cannot delete file as name was not selected."
msgstr "Sitingathe kuchotsa fayilo chifukwa dzina silinasankhidwe."

msgid "Check this to allow"
msgstr "Chongani izi kuti mulole"

msgid "Clear History..."
msgstr "Chotsani Mbiri..."

msgid "Clear Search History"
msgstr "Chotsani Mbiri Yosaka"

msgid "Clear all directories in the \"Folders:\" drop-down list"
msgstr "Chotsani zolemba zonse mu \"Mafoda:\" mndandanda wotsitsa"

msgid "Clear all file names in the \"Files:\" drop-down list"
msgstr "Chotsani mafayilo onse mu fayilo ya \"Mafayilo:\" mndandanda wotsitsa"

msgid "Clear all patterns in the \"Containing:\" drop-down list"
msgstr "Chotsani machitidwe onse mu \"Muli:\" mndandanda wotsitsa"

msgid "Clear containing text"
msgstr "Chotsani mawu"

msgid "Clear file names"
msgstr "Chotsani mayina a mafayilo"

msgid "Clear look in history"
msgstr "Kuyang'ana bwino m'mbiri"

msgid "Configuration"
msgstr "Kusintha"

msgid ""
"Configuration files are for a different version of Searchmonkey. This may "
"cause errors. Delete old configuration files?"
msgstr ""
"Mafayilo osinthira ndi amtundu wina wa Searchmonkey. Izi zitha kuyambitsa "
"zolakwika. Chotsani mafayilo akale osinthira?"

msgid "Containing Text Export Criteria"
msgstr "Muli Zofunikira Zotumizira Kutumiza"

msgid "Containing Text import"
msgstr "Muli Kulowetsa Mawu"

msgid "Containing:"
msgstr "Muli:"

msgid "Context Expression Wizard"
msgstr "Context Expression Wizard"

msgid "Convert Internals"
msgstr "Sinthani Zamkati"

msgid "Copyright (c) 2006-2018 Adam Cottrell"
msgstr "Copyright (c) 2006-2018 Adam Cottrell"

msgid "Couldn't find pixmap file: %s"
msgstr "Sitinapeze fayilo ya pixmap: %s"

msgid "Create a CSV file containing a list of results."
msgstr "Pangani fayilo ya CSV yokhala ndi mndandanda wazotsatira."

msgid "Creates a new row in the list with the following rule."
msgstr "Amapanga mzere watsopano pamndandanda ndi lamulo lotsatirali."

msgid "Credits"
msgstr "Ngongole"

msgid "Csv Export"
msgstr "Csv Export"

msgid "Current Config file location:"
msgstr "Malo a fayilo ya Config pano:"

msgid "Default Applications"
msgstr "Mapulogalamu Ofikira"

msgid "Default folder explorer:"
msgstr "Chosakasaka chikwatu:"

msgid "Default text editor:"
msgstr "Mkonzi wofikira:"

msgid "Default web browser:"
msgstr "Msakatuli wofikira:"

msgid "Delete"
msgstr "Chotsani"

msgid "Deletes an existing row in the rule table."
msgstr "Imachotsa mzere womwe ulipo mu tebulo la malamulo."

msgid "Desktop search to replace find and grep."
msgstr "Kusaka pakompyuta kuti m'malo mwa find ndi grep."

msgid "Did not match any."
msgstr "Sizinafanane ndi iliyonse."

msgid "Discard any changes."
msgstr "Taya zosintha zilizonse."

msgid "Displays the matching text lines when the result is selected."
msgstr "Imawonetsa mizere yofananira pamene chotsatira chasankhidwa."

msgid "Don't know"
msgstr "Sindikudziwa"

msgid ""
"Each row creates a new part to the rule, with the order reflecting the order"
" in which the rule is to be matched."
msgstr ""
"Mzere uliwonse umapanga gawo latsopano ku lamuloli, ndi dongosolo lomwe "
"likuwonetsa dongosolo lomwe lamulolo liyenera kufananizidwa."

msgid "Edit (File)"
msgstr "Sinthani (Fayilo)"

msgid "Edit File"
msgstr "Sinthani Fayilo"

msgid "Enable contents search to find files with matching content too."
msgstr ""
"Yambitsaninso kufufuza zamkati kuti mupeze mafayilo omwe ali ndi zofanana."

msgid "End of row separator:"
msgstr "Mapeto a cholekanitsa mizere:"

msgid "Error saving %s: %s\n"
msgstr "Kusunga zolakwika %s: %s\n"

msgid "Error! Invalid 'containing text' regular expression"
msgstr "Zolakwika! Mawu okhazikika 'okhala ndi mawu' osalondola"

msgid "Error! Invalid 'file name' regular expression"
msgstr "Zolakwika! Mawu okhazikika a 'fayilo' siwolondola"

msgid "Error! Invalid Containing Text regular expression"
msgstr "Zolakwika! Mawu Osavomerezeka Okhazikika"

msgid "Error! Invalid File Name regular expression"
msgstr "Zolakwika! Mawu okhazikika a Dzina la Fayilo"

msgid "Error! Look In directory cannot be blank."
msgstr "Zolakwika! Look Mu chikwatu sichingakhale chopanda kanthu."

msgid "Error! Look In directory is invalid."
msgstr "Zolakwika! Kuyang'ana mu chikwatu ndi cholakwika."

msgid "Error! No valid file selected."
msgstr "Zolakwika! Palibe fayilo yovomerezeka yomwe yasankhidwa."

msgid "Error! Unable to find first combo entry. Is drop down list blank?"
msgstr ""
"Zolakwika! Sitinathe kupeza cholowetsa choyamba. Kodi mndandanda wotsitsa "
"ulibe?"

msgid "Error! Unable to remove config file %s.\n"
msgstr "Zolakwika! Takanika kuchotsa config file %s.\n"

msgid ""
"Error! You must select at least one entry in order to save drop down list."
msgstr ""
"Zolakwika! Muyenera kusankha cholowa chimodzi kuti musunge mndandanda "
"wotsitsa."

msgid "Expert Mode"
msgstr "Katswiri mumalowedwe"

msgid "Export Criteria"
msgstr "Zoyenera Kutumiza kunja"

msgid "Export Expression..."
msgstr "Export Expression..."

msgid "Expression Wizard"
msgstr "Mawu Wizard"

msgid ""
"Extended regular expressions are recommended as they provide more "
"functionality."
msgstr ""
"Mawu owonjezera anthawi zonse amalimbikitsidwa chifukwa akupereka magwiridwe"
" antchito."

msgid "Extras"
msgstr "Zowonjezera"

msgid "File Expression Wizard"
msgstr "Fayilo Expression Wizard"

msgid "File Name Export criteria"
msgstr "Fayilo Name Export criteria"

msgid "File Name import"
msgstr "Kulowetsa Dzina Lafayilo"

msgid "File Selection"
msgstr "Kusankha Fayilo"

msgid "File explorer attributes: %f=filename, %d=directory"
msgstr "Makhalidwe ofufuza mafayilo: %f=dzina lafayilo, %d=directory"

msgid "File name"
msgstr "Dzina lafayilo"

msgid "File size"
msgstr "Kukula kwa fayilo"

msgid "File type"
msgstr "Mtundu wa fayilo"

msgid "File was empty/invalid."
msgstr "Fayilo inali yopanda kanthu/yosalondola."

msgid "Files:"
msgstr "Mafayilo:"

msgid "Find default executables:"
msgstr "Pezani zoyeserera zokhazikika:"

msgid "Folders:"
msgstr "Zikwatu:"

msgid "Follow Symbol _Links"
msgstr "Tsatirani Chizindikiro _Maulalo"

msgid "Follow Symbolic Links (Only on systems that support them)"
msgstr "Tsatirani Maulalo Ophiphiritsa (Pokha pamakina omwe amawathandiza)"

msgid "Force a prompt before any files are deleted"
msgstr "Limbikitsani mwamsanga mafayilo onse achotsedwa"

msgid "Force a prompt before settings are saved."
msgstr "Limbikitsani kufunsa zokonda zisanasungidwe."

msgid "Force single window mode"
msgstr "Limbikitsani mawonekedwe awindo limodzi"

msgid "Found %d matches."
msgstr "Zapezeka %d machesi."

msgid "Found 1 match."
msgstr "Tapeza masewera amodzi."

msgid "Gb"
msgstr "Gb"

msgid "Global Settings"
msgstr "Zokonda Padziko Lonse"

msgid "Grep style file (1-entry, OR each line)"
msgstr "Fayilo ya kalembedwe ka Grep (1-cholowera, KAPENA mzere uliwonse)"

msgid "Help with the Regular Expression Builder"
msgstr "Thandizo ndi Womanga Mawu Okhazikika"

msgid "Highlights per line :"
msgstr "Zowonetsa pamzere uliwonse:"

msgid "Horizontal results"
msgstr "Zotsatira zopingasa"

msgid ""
"If active, display extra lines around the matching line to help show context"
" of match."
msgstr ""
"Ngati ikugwira ntchito, onetsani mizere yowonjezereka mozungulira mzere "
"wofananira kuti muwonetse machesi."

msgid "Ignore hidden files (Unix filesystems only)"
msgstr "Musanyalanyaze mafayilo obisika (mafayilo a Unix okha)"

msgid ""
"Illegal escape sequence: '\\%c'.\n"
" Valid escape characters are: b n r t \" \\"
msgstr ""
"Kuthawa mosaloledwa: '\\%c'.\n"
" Zilembo zovomerezeka zothawa ndi: b n r t \" \\"

msgid "Import Criteria"
msgstr "Zoyenera Kulowetsa"

msgid "Import Expression..."
msgstr "Tengani Mawu..."

msgid "Import a file identical to that generated by the export command."
msgstr ""
"Lowetsani fayilo yofanana ndi yomwe idapangidwa ndi lamulo la kutumiza "
"kunja."

msgid ""
"Input string expands into non ASCII, or multi-byte key sequence: '%s'.\n"
" Please try to use ASCII safe keys"
msgstr ""
"Chingwe cholowetsa chimakula kukhala chosakhala ASCII, kapena makiyi amitundu yambiri: '%s'.\n"
" Chonde yesani kugwiritsa ntchito makiyi otetezeka a ASCII"

msgid "Internal Error! Unable to find calling wizard type!"
msgstr "Cholakwika Chamkati! Sitinathe kupeza mtundu wa wizard yoyimba!"

msgid "Invert Search Expression"
msgstr "Sinthani Mawu Osaka"

msgid "Inverts the file name search results i.e. find all becomes find none."
msgstr ""
"Imatembenuza zotsatira zakusaka kwa dzina lafayilo i.e. find all amakhala "
"osapeza."

msgid "Just save the currently displayed entry to a file."
msgstr "Ingosungani cholowera chomwe chikuwonetsedwa pafayilo."

msgid "Kb"
msgstr "Kb"

msgid "Less than:"
msgstr "Ochepera:"

msgid "Limit Content Highlighting"
msgstr "Chepetsani Kuwunikira Kwazinthu"

msgid "Limit Match Files"
msgstr "Chepetsani Mafayilo Ofananira"

msgid "Limit files found by selecting maximum tree-depth to search."
msgstr "Chepetsani mafayilo opezeka posankha kuzama kwamitengo kuti mufufuze."

msgid "Line Number: %d\n"
msgstr "Nambala Yamzere: %d\n"

msgid "Lines Displayed:"
msgstr "Mizere Yowonetsedwa:"

msgid "List of file(s) corresponding to research criteria."
msgstr "Mndandanda wamafayilo ogwirizana ndi zofufuza."

msgid "Location"
msgstr "Malo"

msgid "Location of your current searchmonkey configuration settings file."
msgstr "Malo omwe muli fayilo yanu yamakono ya searchmonkey configuration."

msgid "Mat_ches"
msgstr "Mat_ches"

msgid "Match Case"
msgstr "Match Case"

msgid "Match strings within binary files too"
msgstr "Fananizani zingwe mumafayilo a binary nawonso"

msgid "Matches"
msgstr "Zofananira"

msgid "Maximum Hits:"
msgstr "Kumenyedwa Kwambiri:"

msgid "Maximum filesize in kBytes/mBytes/gBytes."
msgstr "Kuchuluka kwa fayilo mu kBytes/mBytes/gBytes."

msgid "Mb"
msgstr "Mb"

msgid "Minimum filesize in kBytes/mBytes/gBytes."
msgstr "Mafayilo ocheperako mkati kBytes/mBytes/gBytes."

msgid "Modified"
msgstr "Zosinthidwa"

msgid "Modified date"
msgstr "Tsiku losinthidwa"

msgid "Modified:"
msgstr "Zosinthidwa:"

msgid "Modifies an existing row in the rule table."
msgstr "Imasintha mzere womwe ulipo mu tebulo la malamulo."

msgid "Modify"
msgstr "Sinthani"

msgid "More than:"
msgstr "Kuposa:"

msgid ""
"Multi-byte string entered: '%s'.\n"
"Truncating extra character (%c)"
msgstr ""
"Chingwe cha Multi-byte chalowa: '%s'.\n"
"Kuchepetsa khalidwe lowonjezera (%c)"

msgid "N/A"
msgstr "N/A"

msgid "Name"
msgstr "Dzina"

msgid "New Instance"
msgstr "Chitsanzo Chatsopano"

msgid "No results to save!"
msgstr "Palibe zotsatira zosungidwa!"

msgid "Once"
msgstr "Kamodzi"

msgid "One or more times"
msgstr "Nthawi imodzi kapena zingapo"

msgid "Open Folder"
msgstr "Tsegulani Foda"

msgid "Open a new search window."
msgstr "Tsegulani zenera latsopano lofufuzira."

msgid "Open calendar dialog to choose modified date."
msgstr "Tsegulani zokambirana za kalendala kuti musankhe tsiku losinthidwa."

msgid "Opens a dialog to find a look in directory."
msgstr "Imatsegula zokambirana kuti mupeze mawonekedwe mu chikwatu."

msgid "Options"
msgstr "Zosankha"

msgid "Options: "
msgstr "Zosankha:"

msgid "P_rint Setup..."
msgstr "Kukhazikitsa kwa P_rint..."

msgid "Paragraph Number: %d\n"
msgstr "Nambala ya Ndime: %d\n"

msgid "Paste and edit full file name of your preferred file explorer."
msgstr "Matani ndikusintha dzina lathunthu lafayilo yomwe mumakonda."

msgid "Paste and edit full file name of your preferred text editor."
msgstr "Matani ndikusintha dzina lathunthu lafayilo yomwe mumakonda."

msgid "Paste and edit full file name of your preferred web browser."
msgstr ""
"Matani ndi kusintha dzina lathunthu lafayilo ya msakatuli womwe mumakonda."

msgid ""
"Perform content searching immediately so that results are returned quicker."
msgstr "Chitani kusaka mwachangu kuti zotsatira zibwezedwe mwachangu."

msgid "Phase 1 searching %s"
msgstr "Kusaka kwa gawo 1 %s"

msgid "Phase 2 searching %s"
msgstr "Kusaka kwa Gawo 2 %s"

msgid "Phase 2 starting..."
msgstr "Phase 2 kuyambira..."

msgid ""
"Please be patient!\n"
"\n"
"Searching for executables to perform the following functions:\n"
"  * text editing/viewing\n"
"  * folder browsing\n"
"  * web browsing\n"
"\n"
"The search algorithm uses a set of standard executables for all systems, and then attempts to find them on your local disk.\n"
"\n"
"Once complete, press OK to store the executables."
msgstr ""
"Chonde pirirani!\n"
"\n"
"Kusaka ma executables kuti agwire ntchito zotsatirazi:\n"
"  * kukonza zolemba/kuwona\n"
"  * kusakatula chikwatu\n"
"  * kusakatula pa intaneti\n"
"\n"
"Kusaka kwa algorithm imagwiritsa ntchito njira zoyeserera zamakina onse, ndiyeno kuyesa kuzipeza pa disk yakomweko.\n"
"\n"
"Mukamaliza, dinani OK kuti musunge zomwe zichitike."

msgid "Please email all debug text to cottrela@users.sf.net.\n"
msgstr "Chonde tumizani imelo mawu onse othetsa vutoli cottrela@users.sf.net.\n"

msgid "Please wait..."
msgstr "chonde dikirani..."

msgid ""
"Please, type here a word or a segment of a word researched in the file(s).\n"
"Don't type spaces."
msgstr ""
"Chonde, lembani apa liwu kapena gawo la mawu omwe afufuzidwa mu(ma)fayilo.\n"
"Osalemba mipata."

msgid ""
"Please, type here the name or a segment of the name of the file(s).\n"
"Don't type spaces."
msgstr ""
"Chonde, lembani apa dzina kapena gawo la dzina la mafayilo.\n"
"Osalemba mipata."

msgid "Possibly once"
msgstr "Mwina kamodzi"

msgid "Print results direct to the configured printer."
msgstr "Sindikizani zotsatira molunjika ku printer yokhazikitsidwa."

msgid ""
"Project Manager:\n"
"Adam Cottrell <cottrela@users.sf.net>\n"
"\n"
"Programming:\n"
"  Adam Cottrell <cottrela@users.sf.net>\n"
"  Salil Joshi <HellFeuer@users.sf.net>\n"
"  Yousef AlHashemi\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
"Artwork:\n"
"  Peter Cruickshank\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
msgstr ""
"Woyang'anira ntchito:\n"
"Adam Cottrell <cottrela@users.sf.net>\n"
"\n"
"Kukonza mapulogalamu:\n"
"  Adam Cottrell <cottrela@users.sf.net>\n"
"  Salil Joshi <HellFeuer@users.sf.net>\n"
"  Yousef AlHashemi\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"
"Zojambula:\n"
"  Peter Cruickshank\n"
"  Luc Amimer <amiluc_bis@yahoo.fr>\n"

msgid "Prompt before deleting files"
msgstr "Limbikitsani musanachotse mafayilo"

msgid "Prompt before saving state"
msgstr "Limbikitsani musanasunge dziko"

msgid "Ready-research mode with RegEx"
msgstr "Mwakonzeka kufufuza ndi RegEx"

msgid "Ready-research mode with jokers(DOS like)"
msgstr "Okonzeka-kafukufuku mode ndi nthabwala(DOS monga)"

msgid "Recurse Folders"
msgstr "Recurse Mafoda"

msgid "Release _Notes"
msgstr "Kumasula _Zolemba"

msgid "Remove all saved settings (restart required to complete):"
msgstr ""
"Chotsani zokonda zonse zosungidwa (kuyambitsanso kofunika kuti mumalize):"

msgid "Reset all"
msgstr "Bwezerani zonse"

msgid "Reset size/modified options"
msgstr "Bwezeretsaninso kukula / zosankha zosinthidwa"

msgid "Reset the size and modified search criteria."
msgstr "Bwezeraninso kukula kwake ndi zosinthidwa zosakasaka."

msgid "Restrict depth:"
msgstr "Chepetsani kuya:"

msgid ""
"Run configuration to set default directory/folder browser:\n"
"\"%s\""
msgstr ""
"Thamangani kasinthidwe kuti muyike chikwatu chosasinthika/chikwatu msakatuli:\n"
"\"%s\""

msgid ""
"Run configuration to set default text editor:\n"
"\"%s\""
msgstr ""
"Thamangani kasinthidwe kuti muyike mkonzi wokhazikika:\n"
"\"%s\""

msgid ""
"Run configuration to set default web-browser:\n"
"\"%s\""
msgstr ""
"Thamangani kasinthidwe kuti mukhazikitse msakatuli wokhazikika:\n"
"\"%s\""

msgid "S_ort by"
msgstr "S_ort mwa"

msgid "Save R_esults to CSV..."
msgstr "Sungani R_esults ku CSV..."

msgid "Save Results..."
msgstr "Sungani Zotsatira..."

msgid "Save active/displayed entry only"
msgstr "Sungani zomwe zikugwira / zowonetsedwa zokha"

msgid "Save all entries (1-entry per line)"
msgstr "Sungani zolemba zonse (1-cholowera pamzere uliwonse)"

msgid "Save now"
msgstr "Sungani tsopano"

msgid "Save results to CSV file..."
msgstr "Sungani zotsatira ku fayilo ya CSV..."

msgid "Save settings immediately (normally occurs on exit):"
msgstr "Sungani zoikamo nthawi yomweyo (nthawi zambiri zimachitika potuluka):"

msgid "Save the Containing Text drop down entries into a file."
msgstr "Sungani zolemba za Containing Text mufayilo."

msgid "Save the File Name drop down entries into a file."
msgstr "Sungani zolemba zotsitsa za Dzina la Fayilo mufayilo."

msgid "Search criteria"
msgstr "Zofufuza"

msgid "Search progress"
msgstr "Fufuzani kupita patsogolo"

msgid "Search progress bar."
msgstr "Sakani kapamwamba kapamwamba."

msgid "Searching for system files, please wait..."
msgstr "Mukusaka mafayilo amakina, chonde dikirani..."

msgid "Searchmonkey : search in %s/"
msgstr "Searchmonkey: fufuzani mkati %s/"

msgid "Searchmonkey style (1-entry per line)"
msgstr "Mtundu wa Searchmonkey (1-cholowera pamzere uliwonse)"

msgid "Select Date..."
msgstr "Sankhani Tsiku..."

msgid "Select Font"
msgstr "Sankhani Mafonti"

msgid "Select a folder"
msgstr "Sankhani chikwatu"

msgid "Select highlighting Colour"
msgstr "Sankhani mtundu wowunikira"

msgid "Settings"
msgstr "Zokonda"

msgid "Show Line Contents"
msgstr "Onetsani Zamkatimu Zamzere"

msgid "Show _Statusbar"
msgstr "Onetsani _Statusbar"

msgid "Show _Toolbar"
msgstr "Onetsani _Zida"

msgid "Size"
msgstr "Kukula"

msgid "Size :"
msgstr "Kukula:"

msgid ""
"Some characters (such as . | ^ $) are reserved in certain circumstances.\n"
"\n"
"If you know what you are doing with these characters, unselect this checkbox."
msgstr ""
"Ena otchulidwa (monga . | ^ $) amasungidwa muzochitika zina.\n"
"\n"
"Ngati mukudziwa zomwe mukuchita ndi zilembozi, chotsani kusankha bokosi ili."

msgid "Space(s)"
msgstr "Malo"

msgid ""
"Specify the maximum number of content matches within each file to show. Does"
" not limit the number of files to search."
msgstr ""
"Tchulani kuchuluka kwa zomwe zili mufayilo iliyonse kuti ziwonetsedwe. "
"Sizichepetsa kuchuluka kwa mafayilo oti mufufuze."

msgid ""
"Specify the maximum number of matching files found. Does not limit the "
"number of content matches within each file."
msgstr ""
"Tchulani kuchuluka kwa mafayilo ofananira omwe apezeka. Sizichepetsa "
"kuchuluka kwa zomwe zili mufayilo iliyonse."

msgid "Start"
msgstr "Yambani"

msgid "Start the search!"
msgstr "Yambani kusaka!"

msgid "Stop"
msgstr "Imani"

msgid "Stop the search!"
msgstr "Imitsa kufufuza!"

msgid "Switch search from basic to advanced mode."
msgstr ""
"Sinthani kusaka kuchokera kumayendedwe apamwamba kupita kumayendedwe "
"apamwamba."

msgid "System"
msgstr "Dongosolo"

msgid "Test Regular Expression"
msgstr "Yesani Kulankhula Kwanthawi Zonse"

msgid "Test Regular Expression..."
msgstr "Yesani Kulankhula Kwanthawi Zonse..."

msgid "Text delimiter:"
msgstr "Chotsitsa mawu:"

msgid "Text editor attributes: %f=filename, %d=directory"
msgstr "Makhalidwe osintha mawu: %f=dzina lafayilo, %d=directory"

msgid "The character"
msgstr "Khalidwe"

msgid "The phrase"
msgstr "Mawu akuti"

msgid "This is the expression that will be copied back to the main screen."
msgstr "Awa ndi mawu omwe adzakoperedwe kubwereranso ku chophimba chachikulu."

msgid ""
"To see the results of your regular expression click apply. Once done, simply close the window. Searches are case sensitive.\n"
"\n"
"<b>Note:</b> this tool will highlight <i>each</i> match(es) found."
msgstr ""
"Kuti muwone zotsatira zamawonekedwe anu anthawi zonse dinani Ikani. Mukamaliza, ingotsekani zenera. Zosaka zimakhala zovuta kwambiri.\n"
"\n"
"<b>Zindikirani:</b> chida ichi chidzawunikira <i>aliyense</i> zofananira zapezeka."

msgid "Translated by"
msgstr "Kumasuliridwa ndi"

msgid "Type"
msgstr "Mtundu"

msgid "Unknown"
msgstr "Zosadziwika"

msgid "Update"
msgstr "Kusintha"

msgid "Updates a rule that was selected for the modify function."
msgstr "Kusintha lamulo lomwe lasankhidwa kuti lisinthe."

msgid "Use ASCII characters e.g. \" # $ '"
msgstr "Gwiritsani ntchito zilembo za ASCII mwachitsanzo. \" # $ '"

msgid "Use ASCII or C style construct e.g. , \\t ;"
msgstr "Gwiritsani ntchito kalembedwe ka ASCII kapena C mwachitsanzo. , \\t ;"

msgid "Use ASCII or C style construct e.g. \\n"
msgstr "Gwiritsani ntchito kalembedwe ka ASCII kapena C mwachitsanzo. \\n"

msgid "Use Single Phase Search"
msgstr "Gwiritsani Ntchito Kusaka Kwagawo Limodzi"

msgid "Use Wil_dcard Syntax"
msgstr "Gwiritsani ntchito Wil_dcard Syntax"

msgid "Use _Regular Expression Syntax"
msgstr "Gwiritsani ntchito _Mawu Okhazikika a Syntax"

msgid "Use _Word Wrap"
msgstr "Gwiritsani ntchito _Kukulunga Mawu"

msgid "Use extended regular expressions"
msgstr "Gwiritsani ntchito mawu okhazikika owonjezereka"

msgid "Use regular expression syntax in file search criteria."
msgstr "Gwiritsani ntchito mawu anthawi zonse pofufuza mafayilo."

msgid "Use simple search syntax"
msgstr "Gwiritsani ntchito mawu osavuta osakira"

msgid "Use strict contents case matching."
msgstr "Gwiritsani ntchito zofananira ndi nkhani."

msgid "Use strict file case matching."
msgstr "Gwiritsani ntchito kufananiza kwamafayilo okhwima."

msgid "Use the file in an identical to way to that of the grep -f command."
msgstr "Gwiritsani ntchito fayiloyi mofanana ndi fayilo ya grep -f lamula."

msgid "Use the wizard to create a containing text regular expression..."
msgstr ""
"Gwiritsani ntchito wizard kuti mupange mawu omwe ali ndi mawu okhazikika..."

msgid "Use the wizard to create a file name regular expression..."
msgstr ""
"Gwiritsani ntchito wizard kuti mupange dzina la fayilo nthawi zonse..."

msgid "Use this option to choose the file size units."
msgstr ""
"Gwiritsani ntchito njirayi kuti musankhe mayunitsi a kukula kwa fayilo."

msgid ""
"Use this option to restrict your search results to files containing the "
"provided text (or pattern)."
msgstr ""
"Gwiritsani ntchito njirayi kuti muchepetse zotsatira zakusaka kwanu "
"kumafayilo omwe ali ndi mawu omwe aperekedwa (kapena pateni)."

msgid ""
"Use this option to restrict your search results to files that are bigger "
"than the provided number of kilo Bytes."
msgstr ""
"Gwiritsani ntchito njirayi kuti muchepetse zotsatira zakusaka kwanu "
"kumafayilo akulu kuposa ma kilo Byte omwe mwaperekedwa."

msgid ""
"Use this option to restrict your search results to files that are smaller "
"than the provided number of kilo Bytes."
msgstr ""
"Gwiritsani ntchito njirayi kuti muchepetse zotsatira zakusaka kwanu "
"kumafayilo ang'onoang'ono kuposa ma kilo Byte omwe mwaperekedwa."

msgid ""
"Use this option to restrict your search results to files that were last "
"modified after the provided time."
msgstr ""
"Gwiritsani ntchito njirayi kuti muchepetse zotsatira zakusaka kwanu "
"kumafayilo omwe adasinthidwa nthawi yomaliza itatha."

msgid ""
"Use this option to restrict your search results to files that were last "
"modified before the provided time."
msgstr ""
"Gwiritsani ntchito njirayi kuti muchepetse zotsatira zakusaka kwanu "
"kumafayilo omwe adasinthidwa nthawi yake isanakwane."

msgid "Use this option to search all subfolders (recursively)."
msgstr ""
"Gwiritsani ntchito njirayi kuti mufufuze mafoda onse ang'onoang'ono "
"(mobwerezabwereza)."

msgid "Use wildcard or glob syntax for file search criteria. E.g. *.txt"
msgstr ""
"Gwiritsani ntchito wildcard kapena glob syntax kuti mufufuze mafayilo. "
"Mwachitsanzo *.txt"

msgid "Value/field separator:"
msgstr "Mtengo/Cholekanitsa malo:"

msgid "Vertical results"
msgstr "Zotsatira zoima"

msgid "Write some or all of the search criteria to disk."
msgstr "Lembani zina kapena zonse zakusaka ku disk."

msgid "Written by"
msgstr "Wolemba ndi"

msgid "Zero or more times"
msgstr "Zero kapena kupitilira apo"

msgid "\\\""
msgstr "\\\""

msgid "\\n"
msgstr "\\n"

msgid "_Containing:"
msgstr "_Muli:"

msgid "_Copy (Filename)"
msgstr "_Koperani (dzina lafayilo)"

msgid "_Delete (File)"
msgstr "_Chotsani (Fayilo)"

msgid "_Delete File"
msgstr "_Chotsani Fayilo"

msgid "_Edit"
msgstr "_Sinthani"

msgid "_Explore Folder"
msgstr "_Onani Foda"

msgid "_Export Regular Expression..."
msgstr "_Tumizani Mawonekedwe Okhazikika..."

msgid "_File"
msgstr "_Fayilo"

msgid "_File Name"
msgstr "_Dzina lafayilo"

msgid "_Forums"
msgstr "_Mabwalo"

msgid "_Help"
msgstr "_Thandizeni"

msgid "_Ignore files beginning with '.'"
msgstr "_Musanyalanyaze mafayilo kuyambira '.'"

msgid "_Import Regular Expression..."
msgstr "_Tengani Mawu Okhazikika..."

msgid "_Location"
msgstr "_Malo"

msgid "_Modified"
msgstr "_Zosinthidwa"

msgid "_New Instance"
msgstr "_Chitsanzo Chatsopano"

msgid "_Search"
msgstr "_Sakani"

msgid "_Size"
msgstr "_Kukula"

msgid "_Support"
msgstr "_Thandizo"

msgid "_Type"
msgstr "_Mtundu"

msgid "_User Guide"
msgstr "_Wogwiritsa Ntchito"

msgid "_View"
msgstr "_Onani"

msgid "case sensitive; "
msgstr "zotengera makulidwe azilembo; "

msgid "display %d extra lines around match; "
msgstr "chiwonetsero %d mizere yowonjezera yozungulira machesi; "

msgid "display 1 extra line around match; "
msgstr "Onetsani mzere wowonjezera umodzi kuzungulira machesi; "

msgid "http://searchmonkey.embeddediq.com/index.php/contribute"
msgstr "http://searchmonkey.embeddediq.com/index.php/contribute"

msgid "http://searchmonkey.embeddediq.com/index.php/support"
msgstr "http://searchmonkey.embeddediq.com/index.php/support"

msgid "http://searchmonkey.embeddediq.com/index.php/support/index"
msgstr "http://searchmonkey.embeddediq.com/index.php/support/index"

msgid ""
"http://searchmonkey.sourceforge.net/index.php/Regular_expression_builder"
msgstr ""
"http://searchmonkey.sourceforge.net/index.php/Regular_expression_builder"

msgid "http://sourceforge.net/forum/?group_id=175143"
msgstr "http://sourceforge.net/forum/?group_id=175143"

msgid ""
"https://sourceforge.net/projects/searchmonkey/files/gSearchmonkey%20GTK%20%28Gnome%29/0.8.2%20%5Bstable%5D/"
msgstr ""
"https://sourceforge.net/projects/searchmonkey/files/gSearchmonkey%20GTK%20%28Gnome%29/0.8.2%20%5Bstable%5D/"

msgid "none"
msgstr "palibe"

msgid "only showing first %d content matches; "
msgstr "kuwonetsa koyamba %d zomwe zili zikugwirizana; "

msgid "only showing first content match; "
msgstr "kuwonetsa zofananira zoyambira; "

msgid "only showing line numbers; "
msgstr "kusonyeza manambala amizere okha; "

msgid "research mode with RegEx"
msgstr "njira yofufuzira ndi RegEx"

msgid "research mode with jokers(DOS like)"
msgstr "njira yofufuzira ndi nthabwala (DOS ngati)"

msgid "sample"
msgstr "chitsanzo"

msgid "searchmonkey"
msgstr "searchnyani"

msgid "searchmonkey %s"
msgstr "searchmonkey %s"

msgid "searchmonkey Credits"
msgstr "searchmonkey Credits"

msgid "that occurs"
msgstr "zomwe zimachitika"
